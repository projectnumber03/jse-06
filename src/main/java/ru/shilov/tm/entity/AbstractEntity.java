package ru.shilov.tm.entity;


import java.util.UUID;

public abstract class AbstractEntity {

    private String id = UUID.randomUUID().toString();

    protected AbstractEntity() {
    }

    protected AbstractEntity(AbstractEntity entity) {
        this.id = entity.id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
