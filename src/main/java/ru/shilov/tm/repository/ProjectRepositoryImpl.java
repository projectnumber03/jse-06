package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Project;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements IProjectRepository {

    private static IProjectRepository instance = new ProjectRepositoryImpl();

    public static IProjectRepository getInstance() {
        return instance;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    private ProjectRepositoryImpl() {
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public List<Project> findByUserId(String userId) {
        return projects.values().stream().filter(p -> p.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public Boolean removeByUserId(String userId) {
        return projects.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(userId));
    }

    @Override
    public Project findOne(String id) {
        return findAll().stream().filter(p -> p.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) {
        return projects.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && entry.getValue().getUserId().equals(userId));
    }

    @Override
    public Project persist(Project project) {
        return projects.put(project.getId(), project);
    }

    @Override
    public Project merge(Project project) {
        return projects.put(project.getId(), new Project(project));
    }

    @Override
    public String getId(String value, String userId) {
        List<Project> projects = findByUserId(userId);
        return projects.size() >= Integer.parseInt(value) ? projects.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
