package ru.shilov.tm.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;

import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceImpl implements IProjectService {

    @Override
    public List<Project> findAll() {
        return projectRepo.findAll();
    }

    @Override
    public List<Project> findByUserId(String userId) throws NoSuchEntityException {
        if (isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return projectRepo.findByUserId(userId);
    }

    @Override
    public Boolean removeByUserId(String userId) throws EntityRemoveException {
        if (isNullOrEmpty(userId)) throw new EntityRemoveException();
        return projectRepo.removeByUserId(userId);
    }

    @Override
    public Project findOne(String id) throws NoSuchEntityException {
        Project project = projectRepo.findOne(id);
        if (isNullOrEmpty(id) || project == null) {
            throw new NoSuchEntityException();
        }
        return project;
    }

    @Override
    public void removeAll() {
        projectRepo.removeAll();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        taskRepo.findByUserId(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .map(Task::getId)
                .collect(Collectors.toList())
                .forEach(taskId -> taskRepo.removeOneByUserId(taskId, userId));
        return projectRepo.removeOneByUserId(id, userId);
    }

    @Override
    public Project persist(Project project) throws EntityPersistException {
        if (project == null) throw new EntityPersistException();
        return projectRepo.persist(project);
    }

    @Override
    public Project merge(Project project) throws EntityMergeException {
        if (project == null) throw new EntityMergeException();
        return projectRepo.merge(project);
    }

    @Override
    public String getId(String value, String userId) throws NumberToIdTransformException {
        if (!checkValue(value) || isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return projectRepo.getId(value, userId);
    }

}
