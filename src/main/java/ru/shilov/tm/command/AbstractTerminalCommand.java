package ru.shilov.tm.command;

import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.User;

import java.util.List;

public abstract class AbstractTerminalCommand {

    protected Bootstrap bootstrap;

    public abstract void execute() throws Exception;

    public abstract List<User.Role> getRoles();

    public abstract String getName();

    public abstract String getDescription();

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

}
