package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

import static ru.shilov.tm.context.Bootstrap.DATE_PATTERN;

public class TaskMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = bootstrap.getAuthorizationService().getCurrentUserId();
        Task t = new Task();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        String taskId = bootstrap.getTaskService().getId(bootstrap.getTerminalService().nextLine(), userId);
        t.setId(taskId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ЗАДАЧИ:");
        t.setName(bootstrap.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        t.setDescription(bootstrap.getTerminalService().nextLine());
        try {
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            t.setStart(LocalDate.parse(bootstrap.getTerminalService().nextLine(), DATE_PATTERN));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            t.setFinish(LocalDate.parse(bootstrap.getTerminalService().nextLine(), DATE_PATTERN));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        bootstrap.getTaskService().merge(t);
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Редактирование задачи";
    }

}
