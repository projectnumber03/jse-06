package ru.shilov.tm.context;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.ExitCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.command.project.*;
import ru.shilov.tm.command.task.*;
import ru.shilov.tm.command.user.*;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.error.PermissionException;
import ru.shilov.tm.service.*;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Bootstrap {

    public static DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private TerminalService terminalService = new TerminalService();

    private AuthorizationService authorizationService = new AuthorizationService();

    private IProjectService projectService = new ProjectServiceImpl();

    private ITaskService taskService = new TaskServiceImpl();

    private IUserService userService = new UserServiceImpl();

    private Map<String, AbstractTerminalCommand> commands = initCommands();

    public void init() throws Exception {
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        initUsers();
        while (true) {
            try {
                String commandName = terminalService.nextLine();
                if (!commands.containsKey(commandName)) throw new IllegalCommandException();
                AbstractTerminalCommand command = commands.get(commandName);
                if (!command.getRoles().isEmpty() && !authorizationService.hasAnyRole(command.getRoles())) {
                    throw new PermissionException();
                }
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands() {
        List<AbstractTerminalCommand> commands = Arrays.asList(
                  new TaskFindAllCommand()
                , new ProjectFindAllCommand()
                , new ProjectFindOneCommand()
                , new TaskMergeCommand()
                , new ProjectMergeCommand()
                , new TaskPersistCommand()
                , new ProjectPersistCommand()
                , new TaskRemoveAllCommand()
                , new ProjectRemoveAllCommand()
                , new TaskRemoveCommand()
                , new ProjectRemoveCommand()
                , new TaskSelectCommand()
                , new ProjectSelectCommand()
                , new TaskAttachCommand()
                , new HelpCommand()
                , new ExitCommand()
                , new UserFindAllCommand()
                , new UserLoginCommand()
                , new UserLogoutCommand()
                , new UserMergeCommand()
                , new UserPasswordChangeCommand()
                , new UserRegisterCommand()
                , new UserSelectCommand()
                , new ProjectClearCommand()
                , new TaskClearCommand()
        );
        commands.forEach(tc -> tc.setBootstrap(this));
        return commands.stream().collect(Collectors.toMap(tc -> Objects.requireNonNull(tc).getName(), tc -> tc));
    }

    private void initUsers() throws Exception {
        try {
            userService.persist(new User("user", "123", User.Role.USER));
            userService.persist(new User("admin", "123", User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public TerminalService getTerminalService() {
        return terminalService;
    }

    public AuthorizationService getAuthorizationService() {
        return authorizationService;
    }

    public Map<String, AbstractTerminalCommand> getCommands() {
        return commands;
    }

}
