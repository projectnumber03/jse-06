package ru.shilov.tm.error;

public class NoSuchEntityException extends Exception {

    public NoSuchEntityException() {
        super("Ошибка получения объекта");
    }

}
