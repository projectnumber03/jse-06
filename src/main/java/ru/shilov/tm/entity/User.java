package ru.shilov.tm.entity;

public class User extends AbstractEntity {

    private String login;

    private String password;

    private Role role;

    public User() {
    }

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public User(User user) {
        super(user);
        this.login = user.login;
        this.password = user.password;
        this.role = user.role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Пользователь: ").append(login).append("\n");
        sb.append("Роль: ").append(role.description);
        return sb.toString();
    }

    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        Role(String description) {
            this.description = description;
        }

        private String description;

        public String getDescription() {
            return description;
        }

    }

}
