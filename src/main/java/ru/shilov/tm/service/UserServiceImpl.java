package ru.shilov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;

import java.util.List;

public class UserServiceImpl implements IUserService {

    @Override
    public List<User> findAll() {
        return userRepo.findAll();
    }

    @Override
    public User findOne(String id) throws NoSuchEntityException {
        User user = userRepo.findOne(id);
        if (isNullOrEmpty(id) || user == null) {
            throw new NoSuchEntityException();
        }
        return user;
    }

    @Override
    public void removeAll() {
        userRepo.removeAll();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        return userRepo.removeOneByUserId(id, userId);
    }

    @Override
    public User persist(User user) throws EntityPersistException {
        if (user == null) throw new EntityPersistException();
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        return userRepo.persist(user);
    }

    @Override
    public User merge(User user) throws EntityMergeException {
        if (user == null) throw new EntityMergeException();
        return userRepo.merge(user);
    }

    @Override
    public String getId(String value) throws NumberToIdTransformException {
        if (!checkValue(value)) throw new NumberToIdTransformException(value);
        return userRepo.getId(value);
    }

}
