package ru.shilov.tm.repository;

import ru.shilov.tm.entity.User;

import java.util.*;

public class UserRepositoryImpl implements IUserRepository {

    private static IUserRepository instance = new UserRepositoryImpl();

    public static IUserRepository getInstance() {
        return instance;
    }

    private Map<String, User> users = new LinkedHashMap<>();

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public User findOne(String id) {
        return users.values().stream().filter(entity -> entity.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        users.clear();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) {
        return users.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && !entry.getValue().getId().equals(userId));
    }

    @Override
    public User persist(User user) {
        return users.put(user.getId(), user);
    }

    @Override
    public User merge(User user) {
        return users.put(user.getId(), new User(user));
    }

    @Override
    public String getId(String value) {
        return users.size() >= Integer.parseInt(value) ? new ArrayList<>(users.values()).get(Integer.parseInt(value) - 1).getId() : "";
    }

}
