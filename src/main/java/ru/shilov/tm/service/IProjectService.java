package ru.shilov.tm.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findByUserId(String userId) throws NoSuchEntityException;

    Boolean removeByUserId(String userId) throws EntityRemoveException;

    String getId(String value, String userId) throws NumberToIdTransformException;

}
