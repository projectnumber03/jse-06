package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.User;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

import static ru.shilov.tm.context.Bootstrap.DATE_PATTERN;

public class ProjectMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = bootstrap.getAuthorizationService().getCurrentUserId();
        Project p = new Project();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        String projectId = bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine(), userId);
        p.setId(projectId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
        p.setName(bootstrap.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        p.setDescription(bootstrap.getTerminalService().nextLine());
        try {
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            p.setStart(LocalDate.parse(bootstrap.getTerminalService().nextLine(), DATE_PATTERN));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            p.setFinish(LocalDate.parse(bootstrap.getTerminalService().nextLine(), DATE_PATTERN));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        bootstrap.getProjectService().merge(p);
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Редактирование проекта";
    }

}
