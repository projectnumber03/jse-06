package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Task;
import java.util.*;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements ITaskRepository {

    private static ITaskRepository instance = new TaskRepositoryImpl();

    public static ITaskRepository getInstance() {
        return instance;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    private TaskRepositoryImpl() {
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @Override
    public List<Task> findByUserId(String userId) {
        return tasks.values().stream().filter(t -> t.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public Boolean removeByUserId(String userId) {
        return tasks.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(userId));
    }

    @Override
    public Task findOne(String id) {
        return findAll().stream().filter(t -> t.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) {
        return tasks.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && entry.getValue().getUserId().equals(userId));
    }

    @Override
    public Task persist(Task task) {
        return tasks.put(task.getId(), task);
    }

    @Override
    public Task merge(Task task) {
        return tasks.put(task.getId(), new Task(task));
    }

    @Override
    public String getId(String value, String userId) {
        List<Task> tasks = findByUserId(userId);
        return tasks.size() >= Integer.parseInt(value) ? tasks.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
