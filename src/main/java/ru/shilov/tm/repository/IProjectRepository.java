package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findByUserId(String userId);

    Boolean removeByUserId(String userId);

    String getId(String value, String userId);

}
