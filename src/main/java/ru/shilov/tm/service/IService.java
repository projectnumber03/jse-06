package ru.shilov.tm.service;

import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.repository.*;

import java.util.List;

public interface IService<T> {

    IProjectRepository projectRepo = ProjectRepositoryImpl.getInstance();

    ITaskRepository taskRepo = TaskRepositoryImpl.getInstance();

    IUserRepository userRepo = UserRepositoryImpl.getInstance();

    List<T> findAll();

    T findOne(String id) throws NoSuchEntityException;

    void removeAll();

    Boolean removeOneByUserId(String id, String userId) throws EntityRemoveException;

    T persist(T entity) throws EntityPersistException;

    T merge(T entity) throws EntityMergeException;

    default Boolean isNullOrEmpty(String id) {
        return id == null || id.isEmpty();
    }

    default boolean checkValue(String value) {
        return !isNullOrEmpty(value) && value.matches("\\d+");
    }

}
