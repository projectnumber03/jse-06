package ru.shilov.tm.command.user;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public class UserMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        User u = new User();
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        String userId = bootstrap.getUserService().getId(bootstrap.getTerminalService().nextLine());
        u.setId(userId);
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(bootstrap.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(bootstrap.getTerminalService().nextLine());
        Arrays.asList(User.Role.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.getDescription())));
        System.out.println("ВЫБЕРИТЕ РОЛЬ:");
        String roleId = bootstrap.getTerminalService().nextLine();
        while (!roleCheck(roleId)) {
            System.out.println("ВЫБЕРИТЕ РОЛЬ:");
            roleId = bootstrap.getTerminalService().nextLine();
        }
        u.setRole(User.Role.values()[Integer.parseInt(roleId) - 1]);
        bootstrap.getUserService().merge(u);
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Редактирование пользователя";
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    private boolean roleCheck(String roleId) {
        return roleId != null && !roleId.isEmpty()
                && roleId.matches("\\d+")
                && Integer.parseInt(roleId) <= User.Role.values().length;
    }

}
