package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public class TaskRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = bootstrap.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        String taskId = bootstrap.getTaskService().getId(bootstrap.getTerminalService().nextLine(), userId);
        bootstrap.getTaskService().removeOneByUserId(taskId, userId);
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Удаление задачи";
    }

}
