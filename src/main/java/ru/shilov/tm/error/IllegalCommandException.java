package ru.shilov.tm.error;

public class IllegalCommandException extends Exception {

    public IllegalCommandException() {
        super("Неверная команда");
    }

}
