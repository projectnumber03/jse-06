package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserLoginCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        String login = bootstrap.getTerminalService().nextLine();
        while (!loginCheck(login)) {
            System.out.println("[ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН]");
            System.out.println("ВВЕДИТЕ ЛОГИН:");
            login = bootstrap.getTerminalService().nextLine();
        }
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        String passwd = bootstrap.getTerminalService().nextLine();
        while (!getByLoginPasswd(login, DigestUtils.md5Hex(passwd)).isPresent()) {
            System.out.println("[НЕВЕРНЫЙ ПАРОЛЬ]");
            System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
            passwd = bootstrap.getTerminalService().nextLine();
        }
        bootstrap.getAuthorizationService().setCurrentUser(getByLoginPasswd(login, DigestUtils.md5Hex(passwd)));
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Collections.EMPTY_LIST;
    }

    private boolean loginCheck(String login) {
        return bootstrap.getUserService().findAll().stream().anyMatch(u -> u.getLogin().equals(login));
    }

    private Optional<User> getByLoginPasswd(String login, String passwd) {
        return bootstrap.getUserService().findAll().stream().filter(u -> u.getLogin().equals(login) && u.getPassword().equals(passwd)).findAny();
    }

}
