package ru.shilov.tm.entity;

import java.time.LocalDate;

import static ru.shilov.tm.context.Bootstrap.DATE_PATTERN;

public class Task extends AbstractEntity {

    private String name;

    private String description;

    private LocalDate start;

    private LocalDate finish;

    private String projectId;

    private String userId;

    public Task() {
    }

    public Task(Task task) {
        super(task);
        this.name = task.name;
        this.description = task.description;
        this.start = task.start;
        this.finish = task.finish;
        this.userId = task.userId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Задача: ").append(this.name).append("\n");
        sb.append("Описание: ").append(this.description).append("\n");
        sb.append("Дата начала: ").append(DATE_PATTERN.format(this.start)).append("\n");
        sb.append("Дата окончания: ").append(DATE_PATTERN.format(this.finish));
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
