package ru.shilov.tm.error;

public class EntityMergeException extends Exception {

    public EntityMergeException() {
        super("Ошибка обновления объекта");
    }

}
