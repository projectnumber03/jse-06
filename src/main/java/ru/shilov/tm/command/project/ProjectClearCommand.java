package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityRemoveException;

import java.util.Arrays;
import java.util.List;

public class ProjectClearCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws EntityRemoveException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = bootstrap.getAuthorizationService().getCurrentUserId();
        bootstrap.getProjectService().removeByUserId(userId);
        System.out.println("[ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Удаление всех проектов";
    }

}
