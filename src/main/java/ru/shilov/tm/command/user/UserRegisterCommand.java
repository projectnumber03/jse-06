package ru.shilov.tm.command.user;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;

public class UserRegisterCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        User u = new User();
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(bootstrap.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(bootstrap.getTerminalService().nextLine());
        u.setRole(User.Role.USER);
        bootstrap.getUserService().persist(u);
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String getName() {
        return "register";
    }

    @Override
    public String getDescription() {
        return "Регистрация пользователя";
    }

}
