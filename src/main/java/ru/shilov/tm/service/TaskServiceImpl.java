package ru.shilov.tm.service;

import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;

import java.util.List;

public class TaskServiceImpl implements ITaskService {

    @Override
    public List<Task> findAll() {
        return taskRepo.findAll();
    }

    @Override
    public List<Task> findByUserId(String userId) throws NoSuchEntityException {
        if (isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return taskRepo.findByUserId(userId);
    }

    @Override
    public Boolean removeByUserId(String userId) throws EntityRemoveException {
        if (isNullOrEmpty(userId)) throw new EntityRemoveException();
        return taskRepo.removeByUserId(userId);
    }

    @Override
    public Task findOne(String id) throws NoSuchEntityException {
        Task task = taskRepo.findOne(id);
        if (isNullOrEmpty(id) || task == null) {
            throw new NoSuchEntityException();
        }
        return task;
    }

    @Override
    public void removeAll() {
        taskRepo.removeAll();
    }

    @Override
    public Boolean removeOneByUserId(String id, String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        return taskRepo.removeOneByUserId(id, userId);
    }

    @Override
    public Task persist(Task task) throws EntityPersistException {
        if (task == null) throw new EntityPersistException();
        return taskRepo.persist(task);
    }

    @Override
    public Task merge(Task task) throws EntityMergeException {
        if (task == null) throw new EntityMergeException();
        return taskRepo.merge(task);
    }

    @Override
    public String getId(String value, String userId) throws NumberToIdTransformException {
        if (!checkValue(value) && isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return taskRepo.getId(value, userId);
    }

}
