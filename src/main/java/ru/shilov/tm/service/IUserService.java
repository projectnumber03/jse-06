package ru.shilov.tm.service;

import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NumberToIdTransformException;

public interface IUserService extends IService<User> {

    String getId(String value) throws NumberToIdTransformException;

}
