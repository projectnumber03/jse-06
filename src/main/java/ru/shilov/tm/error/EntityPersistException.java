package ru.shilov.tm.error;

public class EntityPersistException extends Exception {

    public EntityPersistException() {
        super("Ошибка добавления объекта");
    }

}
