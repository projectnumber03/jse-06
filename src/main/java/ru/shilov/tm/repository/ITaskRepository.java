package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findByUserId(String userId);

    Boolean removeByUserId(String userId);

    String getId(String value, String userId);

}
