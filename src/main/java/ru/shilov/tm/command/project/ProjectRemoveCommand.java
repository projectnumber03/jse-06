package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = bootstrap.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        String projectId = bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine(), userId);
        bootstrap.getProjectService().removeOneByUserId(projectId, userId);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
