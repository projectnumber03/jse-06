package ru.shilov.tm.error;

public class NumberToIdTransformException extends Exception {

    public NumberToIdTransformException(String value) {
        super(String.format("Ошибка получения id %s", value));
    }

}
