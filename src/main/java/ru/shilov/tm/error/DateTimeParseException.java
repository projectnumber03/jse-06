package ru.shilov.tm.error;

public class DateTimeParseException extends Exception {

    public DateTimeParseException() {
        super("Некорректная дата");
    }

}
