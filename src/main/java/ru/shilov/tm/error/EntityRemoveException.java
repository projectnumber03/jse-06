package ru.shilov.tm.error;

public class EntityRemoveException extends Exception {

    public EntityRemoveException() {
        super("Ошибка удаления объекта");
    }

}
