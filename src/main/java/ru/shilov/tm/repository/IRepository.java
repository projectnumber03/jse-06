package ru.shilov.tm.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    T findOne(String id);

    void removeAll();

    Boolean removeOneByUserId(String id, String userId);

    T persist(T entity);

    T merge(T entity);

}
