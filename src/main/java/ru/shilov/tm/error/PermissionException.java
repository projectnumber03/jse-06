package ru.shilov.tm.error;

public class PermissionException extends Exception {

    public PermissionException() {
        super("Недостаточно привилегий");
    }

}
