package ru.shilov.tm.repository;

import ru.shilov.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    String getId(String value);

}
