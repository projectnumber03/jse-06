package ru.shilov.tm.service;

import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public class AuthorizationService {

    private Optional<User> currentUser = Optional.empty();

    public String getCurrentUserId() {
        return currentUser.get().getId();
    }

    public void setCurrentUser(Optional<User> currentUser) {
        this.currentUser = currentUser;
    }

    public Boolean hasAnyRole(List<User.Role> roles) {
        return currentUser.isPresent() && roles.contains(currentUser.get().getRole());
    }

}
